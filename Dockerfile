FROM openjdk:8

ADD build/libs/ /code

CMD ["java", "-jar", "/code/app-0.0.1-SNAPSHOT.jar"]
