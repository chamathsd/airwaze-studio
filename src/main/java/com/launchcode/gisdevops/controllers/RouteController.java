package com.launchcode.gisdevops.controllers;

import com.launchcode.gisdevops.data.RouteRepository;
import com.launchcode.gisdevops.features.Feature;
import com.launchcode.gisdevops.features.FeatureCollection;
import com.launchcode.gisdevops.models.Route;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.List;

@Controller
@RequestMapping(value = "/route")
public class RouteController {

    @Autowired
    private RouteRepository routeRepository;

    @RequestMapping(value = "/")
    @ResponseBody
    public FeatureCollection getRoutes() {
        List<Route> routes = routeRepository.findAll();

        FeatureCollection featureCollection = new FeatureCollection();
        for(Route route : routes) {
            HashMap<String, Object> properties = new HashMap<>();
            properties.put("src", route.getSrc());
            properties.put("dst", route.getDst());
            properties.put("airline", route.getAirline());
            featureCollection.addFeature(new Feature(route.getRouteGeom(), properties));
        }

        return featureCollection;
    }
}
