package com.launchcode.gisdevops.data;

import com.launchcode.gisdevops.models.Route;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Transactional
@Repository
public interface RouteRepository extends JpaRepository<Route, Integer> {
    public List<Route> findBySrc(String src);
}