/* global $, ol */

$(document).ready(() => {
    const osmLayer = new ol.layer.Tile({
        source: new ol.source.OSM(),
        visible: true,
    });

    const map = new ol.Map({
        target: 'mapPlaceholder',
        layers: [
            osmLayer,
        ],
        view: new ol.View({
            center: ol.proj.fromLonLat([260.55, 23]),
            zoom: 4,
        }),
    });

    // Define style, source, and layer to show Airport features
    const pointStyle = new ol.style.Style({
        image: new ol.style.Circle({
            radius: 5,
            fill: null,
            stroke: new ol.style.Stroke({ color: 'red', width: 1 }),
        }),
    });
    const airportSource = new ol.source.Vector({
        format: new ol.format.GeoJSON(),
        url: '/airport/',
    });
    const airportLayer = new ol.layer.Vector({
        source: airportSource,
        style() {
            return pointStyle;
        },
    });
    map.addLayer(airportLayer);


    map.on('click', (event) => {
        map.forEachFeatureAtPixel(event.pixel, (feature) => {
            $('#airportList').empty();
            // noinspection JSAnnotator
            $('#airportList').append(`<div id="airportList"><h3>${feature.get('name')}</h3><p>ICAO: ${feature.get('icao')}</p><p>Location: ${feature.get('city')}, ${feature.get('country')}</p><p>Altitude: ${feature.get('alt')}</p><p>Time Zone: ${feature.get('tz')}</p></div>`);
        });
    });
});
