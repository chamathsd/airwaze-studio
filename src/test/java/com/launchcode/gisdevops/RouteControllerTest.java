package com.launchcode.gisdevops;


import com.launchcode.gisdevops.controllers.RouteController;
import com.launchcode.gisdevops.data.RouteRepository;
import com.launchcode.gisdevops.features.WktHelper;
import com.launchcode.gisdevops.models.Route;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@IntegrationTestConfig
public class RouteControllerTest {

    //TODO: remove this test after writing your own
    @Test
    public void emptyTest() {

    }

    @Autowired
    private RouteController routeController;

    @Autowired
    private RouteRepository routeRepository;

    @Autowired
    private MockMvc mockMvc;

    @Before
    public void setup(){
        routeRepository.deleteAll();
    }

   @Test
    public void allOfTheRequiredAttributesAreOnTheJSON() throws Exception {
        routeRepository.save(new Route(WktHelper.wktToGeometry("LINESTRING(45.554194 -122.686101, 45.433001 -122.762632)"), "NZ", 1, "STL", 4, "RAR", 4));
        this.mockMvc.perform(get("/route/"))
                .andExpect(status().isOk())
                .andExpect(jsonPath(("$.features[0].type"), containsString("Feature")))
                .andExpect(jsonPath(("$.features[0].properties.src"), containsString("STL")))
                .andExpect(jsonPath(("$.features[0].properties.dst"), containsString("RAR")))
                .andExpect(jsonPath(("$.features[0].properties.airline"), containsString("NZ")));
    }

    @Test
    public void serverIsUpAndRunning() throws Exception {
        this.mockMvc.perform(get("/route/")).andExpect(status().isOk());
    }

    @Test
    public void oneRouteReturnsOneElement() throws Exception {
        routeRepository.save(new Route(WktHelper.wktToGeometry("LINESTRING(45.554194 -122.686101, 45.433001 -122.762632)"), "NZ", 1, "STL", 4, "RAR", 4));
        this.mockMvc.perform(get("/route/"))
                .andExpect(status().isOk())
                .andExpect(jsonPath(("$.features"), hasSize(1)));
    }

    // TODO: write more route tests. some hints and examples are given
    //public void noRoutesReturnNoEmptySet() throws Exception {
        //this.mockMvc.perform(get("/route/"))


    //public void allOfTheRequiredAttributesAreOnTheJSON() throws Exception {
        //routeRepository.save(new Route(WktHelper.wktToGeometry("LINESTRING(45.554194 -122.686101, 45.433001 -122.762632)"), "NZ", 1, "STL", 4, "RAR", 4));
        //this.mockMvc.perform(get("/route/"))

    //public void routesAreAlwaysLineStrings() throws Exception {


    //public void multipleRoutesAreMultipleFeatures()  throws Exception {

    //public void searchForRoutesFromASpecificAirport() throws Exception {
        //this.mockMvc.perform(get("/route/?srcId=4"))

}
